module ActiveModelSerializers
  module Adapter
    class Json < Base
      def serializable_hash(options = nil)
        options = serialization_options(options)
        serialized_hash = { "data" => Attributes.new(serializer, instance_options).serializable_hash(options) }

        # Pagination root keys
        append_pagination(serialized_hash, meta)

        # Meta keys
        serialized_hash[meta_key] = meta unless meta.blank?

        self.class.transform_key_casing!(serialized_hash, instance_options)
      end

      def meta
        instance_options.fetch(:meta, nil)
      end

      # renamed the default key meta to settings as per mobile app requirement
      def meta_key
        instance_options.fetch(:meta_key, 'settings'.freeze)
      end

      def append_pagination serialized_hash, meta
        if meta.is_a?(Hash) && meta.present?
          serialized_hash[:paging] = meta[:pagination] unless !meta[:pagination].present?
          meta.delete_if{|k,v| ['pagination'].include?(k.to_s) }
        end
      end
    end
  end
end
