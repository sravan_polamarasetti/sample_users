# frozen_string_literal: true

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # extend Devise::Models
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  include DeviseTokenAuth::Concerns::User

  include Filterable
  has_many :taggings, :as => :taggable
  has_many :tags, :through => :taggings
  has_one :blocked_user
  validate :not_a_blocked_user

  def not_a_blocked_user
    if self.email.present?
      unless BlockedUser.find_by_email(self.email).nil?
        errors.add(:email, 'This email has been blocked.')
      end
    end
  end
  
  def clear
    self.blocked_user.build(email: self.email).save
    self.email = "deactivated#{self.id}"
    self.uid = "deactivated#{self.id}"
    self.save(:validate => false)
  end
end
