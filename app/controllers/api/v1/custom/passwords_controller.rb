class Api::V1::Custom::PasswordsController < DeviseTokenAuth::PasswordsController

	def resource_data
    @resource || User.new
  end

  # this action is responsible for generating password reset tokens and
  # sending emails
  def create
    unless resource_params[:email]
      return render_create_error_missing_email
    end

    # give redirect value from params priority
    @redirect_url = params[:redirect_url]

    # fall back to default value if provided
    @redirect_url ||= DeviseTokenAuth.default_password_reset_url

    unless @redirect_url
      return render_create_error_missing_redirect_url
    end

    # if whitelist is set, validate redirect_url against whitelist
    if DeviseTokenAuth.redirect_whitelist
      unless DeviseTokenAuth::Url.whitelisted?(@redirect_url)
        return render_create_error_not_allowed_redirect_url
      end
    end

    # honor devise configuration for case_insensitive_keys
    if resource_class.case_insensitive_keys.include?(:email)
      @email = resource_params[:email].downcase
    else
      @email = resource_params[:email]
    end

    q = "uid = ? AND provider='email'"

    # fix for mysql default case insensitivity
    if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
      q = "BINARY uid = ? AND provider='email'"
    end

    @resource = resource_class.where(q, @email).first

    @errors = nil
    @error_status = 400

    if @resource
      yield @resource if block_given?
      @resource.send_reset_password_instructions({
        email: @email,
        provider: 'email',
        redirect_url: @redirect_url,
        client_config: params[:config_name]
      })

      if @resource.errors.empty?
        return render_create_success
      else
        @errors = @resource.errors
      end
    else
      @errors = [I18n.t("devise_token_auth.passwords.user_not_found", email: @email)]
      @error_status = 200
    end

    if @errors
      return render_create_error
    end
  end

  protected

  def render_create_error_missing_email
    render json: {data: {}, settings: {
      success: false,
      errors: [I18n.t("devise_token_auth.passwords.missing_email")],
      message: I18n.t("devise_token_auth.passwords.missing_email")
    }}, status: 200
  end

  def render_create_error_missing_redirect_url
    render json: {data: {}, settings: {
      success: false,
      errors: [I18n.t("devise_token_auth.passwords.missing_redirect_url")],
      message: I18n.t("devise_token_auth.passwords.missing_redirect_url")
    }}, status: 200
  end

  def render_create_error_not_allowed_redirect_url
    render json: resource_data, meta: {
      status: 'error',
      success: false,
      errors: [I18n.t("devise_token_auth.passwords.not_allowed_redirect_url", redirect_url: @redirect_url)],
      message: I18n.t("devise_token_auth.passwords.not_allowed_redirect_url", redirect_url: @redirect_url)
    }, status: 200
  end

  def render_create_success
    render json: {data: {}, settings: {
      success: true,
      message: I18n.t("devise_token_auth.passwords.sended", email: @email)
    }}
  end

  def render_create_error
    render json: {data: {}, settings: {
      success: false,
      errors: @errors,
      message: @errors.is_a?(Array) ? @errors.join(',') : @errors
    }}, status: @error_status
  end

  def render_edit_error
    raise ActionController::RoutingError.new('Not Found')
  end

  def render_update_error_unauthorized
    render json: {data: {}, settings: {
      success: false,
      errors: ['Unauthorized'],
      message: 'Unauthorized'
    }}, status: 200
  end

  def render_update_error_password_not_required
    render json: {data: {}, settings: {
      success: false,
      errors: [I18n.t("devise_token_auth.passwords.password_not_required", provider: @resource.provider.humanize)],
      message: I18n.t("devise_token_auth.passwords.password_not_required", provider: @resource.provider.humanize)
    }}, status: 200
  end

  def render_update_error_missing_password
    render json: {data: {}, settings: {
      success: false,
      errors: [I18n.t("devise_token_auth.passwords.missing_passwords")],
      message: I18n.t("devise_token_auth.passwords.missing_passwords")
    }}, status: 200
  end

  def render_update_success
    render json: resource_data, meta: {
      success: true,
      message: I18n.t("devise_token_auth.passwords.successfully_updated")
    }
  end

  def render_update_error
    return render json: {data: {}, settings:  {
      success: false,
      errors: resource_errors
    }}, status: 200
  end
end

#https://github.com/lynndylanhurley/devise_token_auth/wiki/Reset-Password-Flow
