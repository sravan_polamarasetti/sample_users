class Api::V1::Custom::RegistrationsController < DeviseTokenAuth::RegistrationsController
  skip_before_action :verify_authenticity_token

  def resource_data
    @resource || User.new
  end

  def create
    @resource            = resource_class.new(sign_up_params)
    @resource.provider   = "email"

    # honor devise configuration for case_insensitive_keys
    if resource_class.case_insensitive_keys.include?(:email)
      @resource.email = sign_up_params[:email].try :downcase
    else
      @resource.email = sign_up_params[:email]
    end


    # give redirect value from params priority
    @redirect_url = params[:confirm_success_url]

    # fall back to default value if provided
    @redirect_url ||= DeviseTokenAuth.default_confirm_success_url

    # success redirect url is required
    if resource_class.devise_modules.include?(:confirmable) && !@redirect_url
      return render_create_error_missing_confirm_success_url
    end

    # if whitelist is set, validate redirect_url against whitelist
    if DeviseTokenAuth.redirect_whitelist
      unless DeviseTokenAuth::Url.whitelisted?(@redirect_url)
        return render_create_error_redirect_url_not_allowed
      end
    end

    begin
      # override email confirmation, must be sent manually from ctrl
      resource_class.set_callback("create", :after, :send_on_create_confirmation_instructions)
      resource_class.skip_callback("create", :after, :send_on_create_confirmation_instructions)

      # if @resource.mobile.present?
      #   @resource.skip_confirmation!
      # end
      # @resource.skip_confirmation!
      # if @resource.jid.present?
      @resource.uid = @resource.email
        if @resource.save

          yield @resource if block_given?
          unless @resource.confirmed?
      
          else
            # email auth has been bypassed, authenticate user
            @client_id = SecureRandom.urlsafe_base64(nil, false)
            @token     = SecureRandom.urlsafe_base64(nil, false)

            @resource.tokens[@client_id] = {token: BCrypt::Password.create(@token),expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i}

            @resource.save!

          end
          render_create_success
        else
          clean_up_passwords @resource
          render_create_error
        end
      # else
      #   if @resource.valid?
      #     render_create_error_jid_cant_be_created
      #   else
      #     clean_up_passwords @resource
      #     render_create_error
      #   end
    # end
    rescue ActiveRecord::RecordNotUnique
      clean_up_passwords @resource
      render_create_error_email_already_exists
    end
  end

  def sign_up_params
    params.permit(:name, :email,:mobile, :password, :password_confirmation, :location, image_attributes: [:pic])
  end

  protected

  def render_create_error_missing_confirm_success_url
    render json: resource_data, meta: {
      status: 'error',
      success: false,
      errors: [I18n.t("devise_token_auth.registrations.missing_confirm_success_url")],
      message: I18n.t("devise_token_auth.registrations.missing_confirm_success_url")
    }, status: 200
  end

  def render_create_error_jid_cant_be_created
    render json: resource_data, meta: {
      status: 'error',
      success: false,
      errors: ["Can't be created as the jabberid cant be created"],
      message: "Can't be created as the jabberid cant be created"
    }, status: 200
  end

  def render_create_error_redirect_url_not_allowed
    render json: resource_data, meta: {
      status: 'error',
      success: false,
      errors: [I18n.t("devise_token_auth.registrations.redirect_url_not_allowed", redirect_url: @redirect_url)],
      message: I18n.t("devise_token_auth.registrations.redirect_url_not_allowed", redirect_url: @redirect_url)
    }, status: 200
  end

  def render_create_success
    render json: resource_data, meta: {
      status: 'success',
      success: true,
      message: resource_data.email.present? ? 'A confirmation email was sent to your email account. You must follow the instructions in the email before your account can be activated.' : 'You are successfully registered.'
    }
  end

  def render_create_error
    render json: resource_data, meta: {
      status: 'error',
      success: false,
      errors: @resource.errors.full_messages,
      message: 'user registration failed.'
    }, status: 200
  end

  def render_create_error_email_already_exists
    render json: resource_data, meta: {
      status: 'error',
      success: false,
      errors: [I18n.t("devise_token_auth.registrations.email_already_exists", email: @resource.email)],
      message: I18n.t("devise_token_auth.registrations.email_already_exists", email: @resource.email)
    }, status: 200
  end

  def render_update_success
    render json: resource_data, meta: {
      status: 'success',
      success: true,
      message: 'user updated successfully'
    }
  end

  def render_update_error
    render json: {data: {}, settings: {
      status: 'error',
      errors: @resource.errors.full_messages,
      success: false,
      message: 'user update failed'
    }}, status: 200
  end

  def render_update_error_user_not_found
    render json: {data: {}, settings: {
      status: 'error',
      success: false,
      errors: [I18n.t("devise_token_auth.registrations.user_not_found")],
      message: I18n.t("devise_token_auth.registrations.user_not_found")
    }}, status: 200
  end

  def render_destroy_success
    render json: {data: {}, settings: {
      status: 'success',
      success: true,
      message: I18n.t("devise_token_auth.registrations.account_with_uid_destroyed", uid: @resource.uid)
    }}
  end

  def render_destroy_error
    render json: {data: {}, settings: {
      status: 'error',
      success: false,
      errors: [I18n.t("devise_token_auth.registrations.account_to_destroy_not_found")],
      message: I18n.t("devise_token_auth.registrations.account_to_destroy_not_found")
    }}, status: 200
  end
end
