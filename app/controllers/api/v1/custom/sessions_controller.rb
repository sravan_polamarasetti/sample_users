class Api::V1::Custom::SessionsController < DeviseTokenAuth::SessionsController
  skip_before_action :verify_authenticity_token

  def create
    # Check
    field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first

    @resource = nil
    if field
      q_value = resource_params[field]

      # if resource_class.case_insensitive_keys.include?(field)
        q_value.downcase!
      # end

      q = "#{field.to_s} = ? AND provider='email'"

      if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
        q = "BINARY " + q
      end

      @resource = resource_class.where(q, q_value).first
    end

    if @resource && valid_params?(field, q_value) && (!@resource.respond_to?(:active_for_authentication?) || @resource.active_for_authentication?)
      valid_password = @resource.valid_password?(resource_params[:password])
      if (@resource.respond_to?(:valid_for_authentication?) && !@resource.valid_for_authentication? { valid_password }) || !valid_password
        render_create_error_bad_credentials
        return
      end
      # create client id
      @client_id = SecureRandom.urlsafe_base64(nil, false)
      @token     = SecureRandom.urlsafe_base64(nil, false)


      @resource.tokens[@client_id] = {
        token: BCrypt::Password.create(@token),
        expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
      }
      @resource.save

      #Storing device tokens
      if resource_params[:device_type] && resource_params[:device_id]
        device = @resource.devices.where(device_type: resource_params[:device_type], device_id: resource_params[:device_id]).first_or_initialize
        device.save if device.new_record? && device.valid?
      end

      sign_in(:user, @resource, store: false, bypass: false)

      yield @resource if block_given?

      render_create_success
    elsif @resource && !(!@resource.respond_to?(:active_for_authentication?) || @resource.active_for_authentication?)
      render_create_error_not_confirmed
    else
      render_create_error_bad_credentials
    end
  end

  protected

  def render_new_error
    render json: {data: {}, settings: {
      success: false,
      errors: [ I18n.t("devise_token_auth.sessions.not_supported")],
      message: I18n.t("devise_token_auth.sessions.not_supported")
    }}, status: 200
  end

  def render_create_success
    render json: @resource, meta: {
      success: true,
      message: 'User Logged In successfully'
    }, status: 200
  end

  def render_create_error_not_confirmed
    render json: {data: {}, settings: {
      success: false,
      errors: [ I18n.t("devise_token_auth.sessions.not_confirmed", email: @resource.email) ],
      message: I18n.t("devise_token_auth.sessions.not_confirmed", email: @resource.email)
    }}, status: 200
  end

  def render_create_error_bad_credentials
    render json: {data: {}, settings: {
      success: false,
      errors: [I18n.t("devise_token_auth.sessions.bad_credentials")],
      message: I18n.t("devise_token_auth.sessions.bad_credentials")
    }}, status: 200
  end

  def render_destroy_success
    render json: {data: {}, settings: {
      success:true,
      message: 'User logged out successfully',
    }}, status: 200
  end

  def render_destroy_error
    render json: {data: {}, settings: {
      success: false,
      errors: [I18n.t("devise_token_auth.sessions.user_not_found")],
      message: I18n.t("devise_token_auth.sessions.user_not_found")
    }}, status: 200
  end

  # def set_password_famews
  #   begin
  #     @user = User.where("email = ? OR mobile = ?", params[:email], params[:email]).first
  #     if @user && @user.register_type == "mobile" && @user.sign_in_count == 0
  #       @user.password = params[:password]
  #       @user.save!
  #     end
  #   rescue Exception => e
  #     render json: {user: {}, meta: {success: false, errors: e.inspect}}, status: 200
  #   end
  # end
end
