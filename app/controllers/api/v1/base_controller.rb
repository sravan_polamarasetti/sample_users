class Api::V1::BaseController < ActionController::Base
    include DeviseTokenAuth::Concerns::SetUserByToken
    protect_from_forgery with: :null_session
    before_action :configure_permitted_parameters, if: :devise_controller?
  
  
    def render_authenticate_error
        return render json: {
            "data": {},
            "settings": {
                "success": false,
                "mesage": I18n.t('devise.failure.unauthenticated'),
            } 
        }
    end

    def check_if_user_edditable
        if current_user.id == params[:id] || current_user.is_admin
            return render json: {
                "data": {},
                "settings": {
                    "success": false,
                    "mesage": 'Can not edit this user',
                } 
            }
        end
    end

    def configure_permitted_parameters
        # devise_parameter_sanitizer.for(:sign_up).push(:name, :location, :confirm_success_url, image_attributes: [:pic])	
        # devise_parameter_sanitizer.permit(:sign_in, keys: [:device_type, :device_id])
    end
end
  
  