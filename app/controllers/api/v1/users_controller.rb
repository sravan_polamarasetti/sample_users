class Api::V1::UsersController < Api::V1::BaseController
    before_action :authenticate_user!, only: [:index,:update, :change_password, :deactivate]
    # before_action :check_user, only: [:index,:update_profile, :change_password, :deactivate]
    before_action :check_default_user
    before_action :check_if_user_edditable, only: [:update, :change_password, :create, :deactivate]

    def index
        if params[:search].present?
            @user = User.where("name ilike ? or email ilike?","%#{params[:search]}%","%#{params[:search]}%")
        else
            @user = User.all
        end
        render :json => @users and return false
    end

    def show
        @user = User.find params[:id]
        render :json => @user and return false
    end

    def create
        @user = User.new(user_params)
        if @user.save
            render json: @user, meta: {success: true, message: 'User created successfully.'}, status: 200
        else
            render json: @user, meta: {success: false, message: @user.errors.full_messages.join(',')}, status: 400
        end
    end

    def update
        @user = User.find params[:id]
        if @user.update_attributes(user_params)
            render json: @user, meta: {success: true, message: 'Profile updated successfully'}, status: 200
        else
            @errors = @user.errors.full_messages
            render json: @user, meta: {success: false, errors: @errors, message: 'Profile update failed'}, status: 200
        end
    end

    def deactivate
        @user = User.find params[:id]
        if @user.blocked_user.present?
            @user.clear
            render json: @user.reload, meta: {success: true, message: 'Account deactivated.'}, status: 200
        else
            render json: @user, meta: {success: true, message: 'Account already deactivated.'}, status: 200
        end
    end
    
    def change_password
        @user = User.find params[:id]
        if @user.valid_password?(change_password_params[:old_password])
          # password = change_password_params[:password]
            @user.password = change_password_params[:password]
             @user.password_confirmation = change_password_params[:password_confirmation]
            if @user.save
                if @user.profile.present?
                    @user.profile.password = change_password_params[:password]
                     @user.save
                end
                render json: @user, meta: {success: true, message: 'Password changed successfully.'}, status: 200
            else
                @errors = @user.errors.full_messages
                render json: @user, meta: {success: false, errors: @errors, message: 'Change password failed.'}, status: 200
            end
        else
            render json: @user, meta: {success: false, message: 'Old password not valid.'}, status: 200
        end
    end

    def add_tag
        @user = User.find params[:id]
        @tag = @user.tag.where(id: params[:tag_id]).first_or_create
        render json: @user, meta: {success: false, message: 'Old password not valid.'}, status: 200
    end

    def destroy
        @user = User.find params[:id]
        if @user.destroy
            render json: {
                "data": @user,
                "settings": {
                    "success": false,
                    "mesage": 'Can not edit this user',
                } 
            }
        else
            render json: {
                "data": {},
                "settings": {
                    "success": false,
                    "mesage": 'Can not edit this user',
                } 
            }
        end
    end

    private

        def user_params
            params.require(:user).permit(:name, :email, :password, :password_confirmation)
        end
  
end