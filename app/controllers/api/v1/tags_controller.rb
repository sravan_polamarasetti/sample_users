class Api::V1::TagsController < Api::V1::BaseController
    def index
        q = ''
        if params[:search].present?
            q += "name ilike '%#{params[:search]}'"
        end
        @tags = Tag.where(q)
        render :json => @tags and return false
    end

    def show
        @tag = Tag.find params[:id]
        render :json => @tag and return false
    end

    def create
        @tag = Tag.new(tag_params)
        if @tag.save
            render json: @tag, meta: {success: true, message: 'tag created successfully.'}, status: 200
        else
            render json: @tag, meta: {success: false, message: @tag.errors.full_messages.join(',')}, status: 400
        end
    end

    def update
        @tag = Tag.find params[:id]
        if @tag.update_attributes(tag_params)
            render json: @tag, meta: {success: true, message: 'Profile updated successfully'}, status: 200
        else
            @errors = @tag.errors.full_messages
            render json: @tag, meta: {success: false, errors: @errors, message: 'Profile update failed'}, status: 200
        end
    end
    
    private
        def tag_params
            params.require(:tag).permit(:name)
        end
end