class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :email,:tags

  def initialize(object, options={})
    super(object, options)
  end

  def tags
    @tags = object.tags
  	@serializer = ActiveModel::SerializableResource.new(@tags, each_serializer: TagSerializer).as_json
  end

end
