class AddIsAdminIsDefaultToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_admin, :boolean
    add_column :users, :is_default, :boolean
  end
end
